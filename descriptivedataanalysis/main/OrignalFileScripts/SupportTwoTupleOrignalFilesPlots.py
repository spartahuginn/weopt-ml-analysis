import os
import numpy as np; np.random.seed(sum(map(ord, "distributions")))
import seaborn as sns; sns.set(color_codes=True)
import pandas as pd
pd.set_option('display.height', 10000)
pd.set_option('display.max_rows', 10000)
pd.set_option('display.max_columns', 10000)
pd.set_option('display.width', 10000)
from pandas import ExcelWriter
import numpy as np
import argparse
import matplotlib.ticker as plticker
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})
plt.style.use('seaborn-whitegrid')
SMALL_SIZE = 12
MEDIUM_SIZE = 15
BIGGER_SIZE = 24

plt.rc('font', size=BIGGER_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title




def calculateSupportForColumnsPairValues(finalDF,outputfolder,action='save'):
        finalDF['R'] = finalDF['R'].apply(lambda x: round(x, 2))
        supportValueCount =finalDF['R'].value_counts().reset_index()
        supportValueCount.columns = ['R','frequency']
        supportValueCount=supportValueCount.sort(['R','frequency'],ascending=[True,False])
        supportValueCount['cumulative frequency'] = supportValueCount['frequency'].cumsum()
        supportValueCount = supportValueCount.reset_index(drop=True)


        output_df_to_file(supportValueCount,'../../resources/finaldata/Two_Tuple_Support_Cumulative','excel')


def draw_cumulative_histogram(data,lastNumber,xlabel, outputFolder,stepsize=2,normalized=False,xlog=False,ylog=False,action='show'):
    fig, ax = plt.subplots(1, figsize=(20, 10))
    noOfBins = round(len(data) / 10)
    sizeOfBins = round(len(data) / noOfBins)
    # plot the cumulative histogram
    n, bins, patches = ax.hist(data, bins=noOfBins, histtype='step',normed=int(normalized),
                                  cumulative=True, label='Empirical',
                               linewidth=3, color='steelblue')
    ax.set_xlabel(xlabel)
    ax.set_ylabel('count')
    if normalized:
        xlog=False
        ylog=False
    else:
        if xlog:
            ax.set_xscale('log')
        if ylog:
            ax.set_yscale('log')
    #ax.set_xlim(min(finalDF.R), max(finalDF.R))
    start, end = ax.get_xlim()
    ax.set_xticks(np.arange(0, end, stepsize))
    ax.set_xticklabels(np.arange(0, end, stepsize), rotation=90)
    ax.xaxis.set_major_formatter(ticker.FormatStrFormatter('%0.1f'))
    title ="".join(["Two Tuple - ", xlabel," vs Count Cumulative"," xlog=",str(xlog)," ylog=",str(ylog)," bin size ", str(sizeOfBins)])
    ax.set_title(title)
    if(action== 'show'):
        plt.show()
    else:
        fig.savefig("".join([outputFolder, title," ",str(lastNumber), ".png"]), dpi=600)
        print("".join([ title," ",str(lastNumber), ".png"]))
        plt.close(fig)


def draw_normal_histogram(data,lastNumber,xlabel, outputFolder,normalized=False,stepsize=2,xlog=False,ylog=False,action='show'):
    fig, ax = plt.subplots(1, figsize=(20, 10))
    # plot normal histogram
    noOfBins= round(len(data)/10)
    sizeOfBins= round(len(data)/noOfBins)
    ax.hist(data, bins=noOfBins,color='steelblue', normed=int(normalized))
    ax.set_xlabel(xlabel)
    ax.set_ylabel('count')
    if normalized:
        xlog=False
        ylog=False
    else:
        if xlog:
            ax.set_xscale('log')
        if ylog:
            ax.set_yscale('log')
    # ax.set_xlim(np.min(finalDF.R), np.max(finalDF.R))
    start, end = ax.get_xlim()
    ax.set_xticks(np.arange(0, end, stepsize))
    ax.set_xticklabels(np.arange(0, end, stepsize), rotation=90)
    ax.xaxis.set_major_formatter(ticker.FormatStrFormatter('%0.1f'))
    title ="".join(["Two Tuple - ", xlabel," vs Count"," xlog=",str(xlog),"ylog=",str(ylog)," bin size ", str(sizeOfBins)])
    ax.set_title(title)

    if(action== 'show'):
        plt.show()
    else:
        fig.savefig("".join([outputFolder, title," ",str(lastNumber), ".png"]), dpi=600)
        print("".join([title," ",str(lastNumber), ".png"]))
        plt.close(fig)


def draw_probability_density_plot(finalDF):
    sns.distplot(finalDF.R, hist=False, rug=True)
    plt.show()


def draw_survival_curve(finalDF):
    fig, ax = plt.subplots(1, 1, figsize=(15, 10))
    tick_spacing = 8
    # evaluate the histogram
    values, base = np.histogram(finalDF.R, bins=len(finalDF['R']))
    # evaluate the cumulative
    cumulative = np.cumsum(values)
    # plot the cumulative function
    # ax.plot(base[:-1], cumulative, c='blue')
    # plot the survival function
    ax.plot(base[:-1], len(finalDF.R) - cumulative, c='green')
    ax.set_xlabel('R')
    ax.set_ylabel('count')
    ax.xaxis.set_major_locator(ticker.MultipleLocator(tick_spacing))

    plt.show()

def get_dataframe_from_file(inputFile,fileFormat):

    if fileFormat.lower() == 'csv':
        df= pd.read_csv(inputFile, index_col=0, sep='\t', encoding='utf-8')
    elif fileFormat.lower() == 'excel':
        df= pd.read_excel(inputFile)
    else:
        df= None
        print("file format should be csv or excel")

    return df

def output_df_to_file(newDf, outputFilePath, outputFormat):

   if outputFormat.lower() == "csv":
        defaultCsvOutputFilePath = "".join([outputFilePath, ".", "csv"])
        newDf.to_csv(defaultCsvOutputFilePath, sep='\t', encoding='utf-8')

   else:
        defaultStatsExcelFilePath = "".join([outputFilePath, ".", "xlsx"])
        writer = ExcelWriter(defaultStatsExcelFilePath)
        newDf.to_excel(writer, 'Sheet1')
        writer.save()


if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--inputfile", required=True,
                    help="the file should be All_files_combined.<csv/xlsx>")

    ap.add_argument("-inf", "--inputformat", required=True,
                    help="format of input file <csv/xlsx>")

    ap.add_argument("-o", "--outputaction", required=True,
                    help="save plots as pics or draw plots on screen. <show/save>")

    ap.add_argument("-outf", "--outputfolder", required=True,
                    help="folder where plots are stored")

    args = vars(ap.parse_args())

    #--inputfile ../../resources/orignaldataallfiles/Two_Tuple_Support_Main_File.xlsx --inputformat excel --outputaction save --outputfolder ../../resources/figuresorignaldata/twotuplefigures/
    finalDF = get_dataframe_from_file(args['inputfile'], args['inputformat'])


    #calculateSupportForColumnsPairValues(finalDF,args['outputfolder'],action = 'save')
    #draw_probability_density_plot(finalDF)
    #draw_survival_curve(finalDF)

    supportData = finalDF.R[finalDF.R < 10]
    draw_normal_histogram(supportData,1,"R",os.path.join(args['outputfolder'], ''),normalized=False,stepsize=0.2, xlog=False, ylog=True, action='save')
    draw_cumulative_histogram(supportData,2,"R",os.path.join(args['outputfolder'], ''),normalized=False,stepsize=0.2, xlog=False, ylog=True, action='save')
    draw_cumulative_histogram(supportData, 3, "R",os.path.join(args['outputfolder'], ''), normalized=True, stepsize=0.2, xlog=False, ylog=True, action='save')

    freqData = finalDF['frequency of tuple']
    draw_normal_histogram(freqData, 4,"Frequency of tuple",os.path.join(args['outputfolder'], ''),normalized=False,stepsize=1000, xlog=False, ylog=True, action='save')
    draw_cumulative_histogram(freqData, 5,"Frequency of tuples",os.path.join(args['outputfolder'], ''),normalized=False,stepsize=1000, xlog=False, ylog=True, action='save')
    draw_cumulative_histogram(freqData, 6, "Frequency of tuples",os.path.join(args['outputfolder'], ''), normalized=True, stepsize=1000, xlog=False,ylog=True, action='save')



