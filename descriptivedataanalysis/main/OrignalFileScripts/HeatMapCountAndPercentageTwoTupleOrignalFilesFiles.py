import os
import numpy as np; np.random.seed(0)
import seaborn as sns; sns.set()
import pandas as pd
pd.set_option('display.height', 10000)
pd.set_option('display.max_rows', 10000)
pd.set_option('display.max_columns', 10000)
pd.set_option('display.width', 10000)
import argparse
import matplotlib.pyplot as plt
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})


def makeHeatMapsForColumnsPairGroupbyCount(inputFile,inputKeys, action='save',inputfileformat='excel',keysfileformat='excel',savefigaddress="./" ):

    df_in = get_dataframe_from_file(inputFile,inputfileformat)
    df_in = df_in.fillna('EMPTY')
    df_keys = get_dataframe_from_file(inputKeys,keysfileformat)

    df_in = df_in[df_keys['atomickeys'].dropna()]

    for col in df_in.columns:
        if len(df_in[col].unique()) > 25:
            df_in.loc[df_in[col] != 'EMPTY', col] = "PRESENT"

    for ind1,col1 in enumerate(df_in.columns):
        for ind2, col2 in enumerate(df_in.columns):
            if ind2 > ind1:
                #df_new = df_in.filter([col1, col2], axis=1)
                df_new = df_in.groupby([col1, col2]).size()
                df_new = df_new.reset_index(name="Count")
                df_new = df_new.pivot(col1, col2, 'Count')
                plt.figure(figsize=(15,15))

                ax = sns.heatmap(df_new, annot=True,fmt=".0f",linewidths=.7, cmap="RdYlGn")
                plt.title(col1 +  "-" + col2)
                plt.yticks(rotation=60)
                plt.xticks(rotation=60)

                if action.lower() == 'show':
                    plt.show()
                else:
                    fig = plt.gcf()
                    fig.savefig("".join([savefigaddress, col1 +  "-" + col2, ".png"]), dpi=500)
                    plt.close(fig)

    # ['Approved by' 'Checked by' 'Created by' 'Current State' 'Design Origin'
#  'KONE Type' 'Last modification time' 'Level of Pre-engineering'
#  'Material Utilisation Group' 'Object ID' 'Unit of measure'
#  'characteristics' 'created_final_data_file' 'has part' 'material_id'
#  'spare_part' 'sub_materials_released']

def makeHeatMapsForColumnsPairGroupbyCountPercentage(inputFile,inputKeys, action='save',inputfileformat='excel',keysfileformat='excel',savefigaddress="./" ):
    df_in = get_dataframe_from_file(inputFile, inputfileformat)
    df_in = df_in.fillna('EMPTY')
    df_keys = get_dataframe_from_file(inputKeys, keysfileformat)

    df_in = df_in[df_keys['atomickeys'].dropna()]

    for col in df_in.columns:
        if len(df_in[col].unique()) > 25:
            df_in.loc[df_in[col] != 'EMPTY', col] = "PRESENT"

    for ind1,col1 in enumerate(df_in.columns):
        for ind2, col2 in enumerate(df_in.columns):
            if ind2 > ind1:
                #df_new = df_in.filter([col1, col2], axis=1)
                df_new = df_in.groupby([col1, col2]).size()
                df_new = df_new.reset_index(name="Count")
                df_new['Percent Count'] = 100 * df_new['Count']/df_new['Count'].sum()
                df_new = df_new.pivot(col1, col2, 'Percent Count')
                plt.figure(figsize=(15,15))

                ax = sns.heatmap(df_new, annot=True,fmt=".2f",linewidths=.7, cmap="RdYlGn")
                plt.title(col1 +  "-" + col2)
                plt.yticks(rotation=60)
                plt.xticks(rotation=60)

                if action.lower() == 'show':
                    plt.show()
                else:
                    fig = plt.gcf()
                    fig.savefig("".join([savefigaddress, col1 +  "-" + col2, ".png"]), dpi=500)
                    plt.close(fig)



def get_dataframe_from_file(inputFile,fileFormat):

    if fileFormat.lower() == 'csv':
        df= pd.read_csv(inputFile, index_col=0,sep='\t', encoding='utf-8')
    elif fileFormat.lower() == 'excel':
        df= pd.read_excel(inputFile)
    else:
        df= None
        print("file format should be csv or excel")

    return df



if __name__ == '__main__':

    # makeHeatMapsForColumnsPairGroupbyCount('../../resources/orignaldataallfiles/All_files_combined.xlsx',
    #                            '../../resources/orignaldatakeys/catogoriesofkeys.xlsx',
    #                            action="save",inputfileformat='excel',keysfileformat='excel',
    #                            savefigaddress='../../resources/figuresorignaldata/heatmaps/')
    # construct the argument parse and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--inputfile", required=True,
                    help="one input file in which all final files combined")
    ap.add_argument("-ik", "--inputfilekeys", required=True,
                    help="one input file for keys to be considered")

    ap.add_argument("-inf", "--inputformat", required=True,
                    help="format of input file <csv/xlsx>")
    ap.add_argument("-inkf", "--inputkeysformat", required=True,
                    help="format of keys input file <csv/xlsx>")

    ap.add_argument("-o", "--outputaction", required=True,
                    help="save plots as pics or draw plots on screen. <show/save>")

    ap.add_argument("-outf", "--outputfolder", required=True,
                    help="folder where plots are stored")

    args = vars(ap.parse_args())
    #--inputfile ../../resources/orignaldataallfiles/All_files_combined.xlsx --inputfilekeys ../../resources/orignaldatakeys/catogoriesofkeys.xlsx --inputformat excel --inputkeysformat excel --outputaction save --outputfolder ../../resources/figuresorignaldata/heatmapspercentages/
    makeHeatMapsForColumnsPairGroupbyCount(args['inputfile'],args['inputfilekeys'], action=args['outputaction'],
                                           inputfileformat=args['inputformat'],keysfileformat=args['inputkeysformat'],savefigaddress= os.path.join(args['outputfolder'], ''))
    makeHeatMapsForColumnsPairGroupbyCountPercentage(args['inputfile'],args['inputfilekeys'], action=args['outputaction'],
                                           inputfileformat=args['inputformat'],keysfileformat=args['inputkeysformat'],savefigaddress= os.path.join(args['outputfolder'], ''))

    # makeHeatMapsForColumnsPairGroupbyCountPercentage('../../resources/orignaldataallfiles/All_files_combined.xlsx',
    #                            '../../resources/orignaldatakeys/catogoriesofkeys.xlsx',
    #                            action="save",inputfileformat='excel',keysfileformat='excel',
    #                            savefigaddress='../../resources/figuresorignaldata/heatmapspercentages/')
