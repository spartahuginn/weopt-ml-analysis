import os

import pandas as pd
pd.set_option('display.height', 10000)
pd.set_option('display.max_rows', 10000)
pd.set_option('display.max_columns', 10000)
pd.set_option('display.width', 10000)
import argparse
import pickle
from tabulate import tabulate
import sys
import json
from jsonFlattner.flatten_json import flatten
from pandas import ExcelWriter
from pandas import ExcelFile
from pandas.io.common import EmptyDataError
import numpy as np
from functools import reduce
import matplotlib.pyplot as plt
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})

import logging

# Create the Logger
logger = logging.getLogger("Final_Data")
logger.setLevel(logging.INFO)

# Create a Formatter for formatting the log messages
logger_formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s')

# Create the Handler for logging data to a file
logger_file_handler = logging.FileHandler('orignal_data_crunching.log')
# Add the Formatter to the Handler
logger_file_handler.setFormatter(logger_formatter)
# Add the Handler to the Logger
logger.addHandler(logger_file_handler)


logger_console_Handler = logging.StreamHandler()
logger_console_Handler.setFormatter(logger_formatter)
logger.addHandler(logger_console_Handler)


logger.info('Completed configuring logger()!')



def collect_frequenecies_multiple_orignal_data_file(inputDirPath, outputFormat="csv"):
    try:
        allfiles = os.listdir(inputDirPath)
    except FileNotFoundError:
        logger.error("The system cannot find the path specified " + inputDirPath)
        exit(1)


    listFlattenResultsDict = []  # contain flatten json from all rows in file
    filenameToKeyCountSeries = {}
    globalResultAttributeKeys = get_global_keys_as_list()
    for filename in allfiles:
        if os.path.isfile(os.path.join(inputDirPath, filename)) and 'original_data_' in filename and filename.endswith(".csv"):
            inputFilePath= os.path.join(inputDirPath, filename)
            try:
                OrignalDataCsv = pd.read_csv(inputFilePath, sep=';',encoding='utf-8')
                logger.info("Successfully  reading file " + filename)
            except EmptyDataError:
                logger.info("Unsuccessful read file " + filename + " because empty")
                continue
            rowcounter = len(OrignalDataCsv.index)
            #print(rowcounter)
            counts = pd.Series()
            try:
                for i, j in OrignalDataCsv.iterrows():
                    jsonResult = json.loads(j['result_attributes'])

                    jsonResultPruned = { your_key: jsonResult[your_key] for your_key in globalResultAttributeKeys }
                    #flattenedJsonResultDict = flatten(jsonResultPruned) # contain flatten json in one row of file
                    flattenedJsonResultDict = jsonResultPruned
                    for key in flattenedJsonResultDict.keys():
                        counts[key] = counts.get(key, 0) + 1

                    flattenedJsonResultDict['created_orignal_data_file'] = filename
                    listFlattenResultsDict.append(flattenedJsonResultDict)
                filenameToKeyCountSeries[filename]=counts
                logger.info("Successfully parsed " + filename)
            except KeyError:
                logger.error("Aborting parsing because got missing key in json")
        else:
            continue


    df = pd.DataFrame(listFlattenResultsDict)

    write_combined_files_as_dataframe_to_file(df, inputDirPath, outputFormat)

    write_accumulative_statistics_multiple_file(df, filenameToKeyCountSeries, inputDirPath, outputFormat)

    write_statistics_per_file_multiple_file(df, filenameToKeyCountSeries, inputDirPath, outputFormat)
        #print(tabulate(newDF, headers='keys', tablefmt='psql'))

def write_combined_files_as_dataframe_to_file(df, inputDirPath, outputFormat):
    outputFilePath = "".join([inputDirPath, "All_files_combined"])
    output_df_to_file(df, outputFilePath, outputFormat)

def write_accumulative_statistics_multiple_file(df, filenameToKeyCountSeries, inputDirPath, outputFormat):
    uniqueFileNames = df.created_orignal_data_file.unique()
    statsFrames = []
    for uniqueName in uniqueFileNames:  # generate count for unique file names
        uniqueDf = df[df['created_orignal_data_file'] == uniqueName]
        uniqueRowCounter = len(uniqueDf.index)
        #print(uniqueRowCounter)

        statsDf = multiple_file_generate_accumulative_statistics(filenameToKeyCountSeries.get(uniqueName),
                                                                 uniqueDf,
                                                                 uniqueName, uniqueRowCounter,
                                                                 ['created_orignal_data_file'])

        # print(tabulate(statsDf, headers='keys', tablefmt='psql'))
        statsFrames.append(statsDf)
    newDf = reduce(lambda x, y: x.add(y, fill_value=0), statsFrames)

    newDf.loc['percent value count'] = newDf.loc['value counts'] / newDf.loc['key counts'] * 100
    newDf.loc['percent key count'] = newDf.loc['key counts'] / newDf.loc['total rows'] * 100
    outputFilePath= "".join([inputDirPath,"Accumulative_files_stats"])
    output_df_to_file(newDf,outputFilePath, outputFormat)


def output_df_to_file(newDf, outputFilePath, outputFormat):

    if outputFormat.lower() == "csv":
        defaultCsvOutputFilePath = "".join([outputFilePath, ".", "csv"])
        newDf.to_csv(defaultCsvOutputFilePath, sep='\t', encoding='utf-8')

    else:
        defaultStatsExcelFilePath = "".join([outputFilePath, ".", "xlsx"])
        writer = ExcelWriter(defaultStatsExcelFilePath)
        newDf.to_excel(writer, 'Sheet1')
        writer.save()


def multiple_file_generate_accumulative_statistics(keyCounts, df, fileBaseName, rowcounter, listOfColsToBeDeleted):
    countStatsForSpecificFile = df.count()# no of values present .
    countStatsForSpecificFile = countStatsForSpecificFile.drop(
        labels=listOfColsToBeDeleted)
    countTotalRows = pd.Series(rowcounter,  index= countStatsForSpecificFile.keys())
    statsDf = pd.DataFrame([countStatsForSpecificFile, keyCounts,countTotalRows], index=['value counts', 'key counts', 'total rows'])
    #statsDf=statsDf.iloc[:, np.argsort(-statsDf.iloc[3])]# iloc[ row selection , column slection].. here np.argsort gives sorted column indices for row 4 i.e. percentage key count
    return statsDf


def write_statistics_per_file_multiple_file(df, filenameToKeyCountSeries, inputDirPath, outputFormat):
    uniqueFileNames = df.created_orignal_data_file.unique()
    statsFrames = []
    for uniqueName in uniqueFileNames:  # generate count for unique file names
        uniqueDf = df[df['created_orignal_data_file'] == uniqueName]
        uniqueRowCounter = len(uniqueDf.index)
        #print(uniqueRowCounter)


        statsDf = multiple_file_generate_statistics(filenameToKeyCountSeries.get(uniqueName), uniqueDf,
                                                    uniqueName, uniqueRowCounter, ['created_orignal_data_file'])
        # print(tabulate(statsDf, headers='keys', tablefmt='psql'))
        statsFrames.append(statsDf)
    newDf = pd.concat(statsFrames, ignore_index=True)
    outputFilePath = "".join([inputDirPath, "All_files_stats"])
    output_df_to_file(newDf, outputFilePath, outputFormat)



def multiple_file_generate_statistics(keyCounts, df, fileBaseName, rowcounter, listOfColsToBeDeleted):
    countStatsForSpecificFile = df.count()# no of values present .
    countStatsForSpecificFile = countStatsForSpecificFile.drop(labels=listOfColsToBeDeleted)

    percentStatsForSpecificFile = pd.Series() # (total no of values present/ total no of key present)  * 100
    for i,v in countStatsForSpecificFile.iteritems():
        percentStatsForSpecificFile[i]= v / keyCounts[i] * 100



    #countStatsForSpecificFile = countStatsForSpecificFile.drop(labels=listOfColsToBeDeleted)
    #percentStatsForSpecificFile = percentStatsForSpecificFile.drop(labels=listOfColsToBeDeleted)

    percentKeyCountStatsForSpecificFile = (keyCounts.apply(lambda x: x / rowcounter * 100))# (total no of rows with key present / total rows) * 100

    statsDf = pd.DataFrame([countStatsForSpecificFile, percentStatsForSpecificFile, keyCounts, percentKeyCountStatsForSpecificFile])
    #statsDf=statsDf.iloc[:, np.argsort(-statsDf.iloc[3])]# iloc[ row selection , column slection].. here np.argsort gives sorted column indices for row 4 i.e. percentage key count

    statsDf.insert(loc=0, column="generated_stats_type", value=['value count', 'percentage value count', 'key count', 'percentage key count'])
    sLength = len(statsDf)
    statsDf.insert(loc=0, column="generated_source_file_for_stats", value=[fileBaseName] * sLength)

    return statsDf



def get_global_keys_as_list():
    #with open('../../resources/orignaldatakeys/listofglobalkeys.csv', 'r') as f:
        #reader = csv.reader(f)
        #itemlist1 = list(reader)[0]




    with open('../../resources/orignaldatakeys/listofglobalkeys', 'rb') as fp:
        itemlist = pickle.load(fp)
        return itemlist

def collect_frequencies_single_orignal_data_file(inputFilePath, outputFormat="csv"):
    try:
       OrignalDataCsv = pd.read_csv(inputFilePath, sep=';', encoding='utf-8')
    except EmptyDataError:
        print("ERROR READING FILE " + inputFilePath)
        return

    rowcounter = len(OrignalDataCsv.index)

    counts = pd.Series()
    listFlattenResultsDict = []
    for i, j in OrignalDataCsv.iterrows():

        jsonResult = json.loads(j['result_attributes'])#null in json convert to None which is NaN in pandas dataframe
        flattenedJsonResultDict = flatten(jsonResult)

        for key in flattenedJsonResultDict.keys():
            counts[key] = counts.get(key, 0) + 1
        listFlattenResultsDict.append(flattenedJsonResultDict)# different size dicts also introduce NaN in data frame

    fileBaseName = os.path.basename(inputFilePath)
    filepath, ext = os.path.splitext(inputFilePath)

    if outputFormat.lower()=="csv":
        df = pd.DataFrame(listFlattenResultsDict)
        defaultCsvOutputFilePath = "".join([filepath, ".", "csv"])
        df.to_csv(defaultCsvOutputFilePath, sep='\t', encoding='utf-8')

        statsDf = orignal_file_statistics_generation(counts, df, fileBaseName,rowcounter)

        defaultStatsCsvFilePath = "".join([filepath, "_stats",".", "csv"])
        statsDf.to_csv(defaultStatsCsvFilePath, sep='\t', encoding='utf-8')

    else:

        df = pd.DataFrame(listFlattenResultsDict)
        defaultExcelOutputFilePath = "".join([filepath, ".", "xlsx"])
        writer = ExcelWriter(defaultExcelOutputFilePath)
        df.to_excel(writer, 'Sheet1', index=False)
        writer.save()

        statsDf = orignal_file_statistics_generation(counts, df, fileBaseName,rowcounter)
        defaultStatsExcelFilePath = "".join([filepath, "_stats", ".", "xlsx"])
        writer = ExcelWriter(defaultStatsExcelFilePath)
        statsDf.to_excel(writer, 'Sheet1', index=False)
        writer.save()

    # print(tabulate(newDF, headers='keys', tablefmt='psql'))
def orignal_file_statistics_generation(keyCounts, df, fileBaseName, rowcounter):
    countStatsForSpecificFile = df.count()# no of values present .

    percentStatsForSpecificFile = pd.Series() # (total no of values present/ total no of key present)  * 100
    for i,v in countStatsForSpecificFile.iteritems():
        percentStatsForSpecificFile[i]= v / keyCounts.get(i,1) * 100


    percentKeyCountStatsForSpecificFile = (keyCounts.apply(
        lambda x: x / rowcounter * 100))# (total no of rows with key present / total rows) * 100
    statsDf = pd.DataFrame([countStatsForSpecificFile, percentStatsForSpecificFile, keyCounts, percentKeyCountStatsForSpecificFile])
    statsDf=statsDf.iloc[:, np.argsort(-statsDf.iloc[3])]# iloc[ row selection , column slection].. here np.argsort gives sorted column indices for row 4 i.e. percentage key count
    statsDf.insert(loc=0, column="generated_stats_type", value=['value count', 'percentage value count', 'key count', 'percentage key count'])
    sLength = len(statsDf)
    statsDf.insert(loc=0, column="generated_source_file_for_stats", value=[fileBaseName] * sLength)
    return statsDf





if __name__ == '__main__':
    # construct the argument parse and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-f", "--folder", required=True,
                    help="folder with all final files in csv format")

    ap.add_argument("-o", "--outputformat", required=True,
                    help="format (csv or xslx) in which to generate output files. default is csv. If erronous defaults to xlsx")

    args = vars(ap.parse_args())

    #python DescriptiveDataAnalysisFinalFiles.py --folder ../../resources/orignaldataallfiles/ --outputformat csv

    collect_frequenecies_multiple_orignal_data_file(os.path.join(args['folder'], ''), outputFormat=args["outputformat"])

exit(0)
