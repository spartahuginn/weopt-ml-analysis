import os
import pandas as pd
pd.set_option('display.height', 10000)
pd.set_option('display.max_rows', 10000)
pd.set_option('display.max_columns', 10000)
pd.set_option('display.width', 10000)
import argparse
import matplotlib.pyplot as plt
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})


def makeHistogramForAllColumns(inputFile,inputKeys, action='save',inputfileformat='excel',keysfileformat='excel',savefigaddress="./" ):
    df_in = get_dataframe_from_file(inputFile,inputfileformat)
    df_in = df_in.fillna('EMPTY')
    df_keys = get_dataframe_from_file(inputKeys,keysfileformat)
    counter = 0
    for key in df_keys['atomickeys'].dropna():
        if len(df_in[key].unique())<100:
            plot_string_value_count(df_in[key].value_counts(), key, action, savefigaddress)
            counter+=1
            print(counter)
        else:
            df_in.loc[df_in[key] != 'EMPTY', key] = "PRESENT"
            plot_string_value_count(df_in[key].value_counts(), key, action, savefigaddress)
            counter += 1
            print(counter)



def plot_string_value_count(countSeries,plotTitle,action,savefigaddress):
    print("plotting " + plotTitle)

    values = countSeries.index.tolist()
    valuesOccurances = countSeries.values
    # bars are by default width 0.8, so we'll add 0.1 to the left coordinates
    # so that each bar is centered
    xs = [i + 0.1 for i, _ in enumerate(values)]
    plt.figure(figsize=(25, 15))

    # plot bars with left x-coordinates [xs], heights [valuesOccurances]
    plt.bar(xs, valuesOccurances,color='blue')
    plt.ylabel("# of Value Occurances")
    plt.title(plotTitle + " - " + " Distinct Items " + str(len(valuesOccurances)))
    # label x-axis with movie names at bar centers
    plt.xticks([i + 0.1 for i, _ in enumerate(values)], values, rotation='vertical', fontsize = 8)
    # Pad margins so that markers don't get clipped by the axes
    plt.margins(0.01)
    # Tweak spacing to prevent clipping of tick-labels
    plt.subplots_adjust(bottom=0.15)

    if action.lower() == 'show':
        mng = plt.get_current_fig_manager()
        mng.window.showMaximized()
        plt.show()
    else:
        fig = plt.gcf()
        fig.savefig("".join([savefigaddress,plotTitle,".png"]),dpi=600)
        plt.close(fig)



def get_dataframe_from_file(inputFile,fileFormat):

    if fileFormat.lower() == 'csv':
        df= pd.read_csv(inputFile, index_col=0, sep='\t', encoding='utf-8')
    elif fileFormat.lower() == 'excel':
        df= pd.read_excel(inputFile)
    else:
        df= None
        print("file format should be csv or excel")

    return df


if __name__ == '__main__':
    # construct the argument parse and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--inputfile", required=True,
                    help="one input file in which all final files combined")
    ap.add_argument("-ik", "--inputfilekeys", required=True,
                    help="one input file for keys to be considered")

    ap.add_argument("-inf", "--inputformat", required=True,
                    help="format of input file <csv/xlsx>")
    ap.add_argument("-inkf", "--inputkeysformat", required=True,
                    help="format of keys input file <csv/xlsx>")

    ap.add_argument("-o", "--outputaction", required=True,
                    help="save plots as pics or draw plots on screen. <show/save>")

    ap.add_argument("-outf", "--outputfolder", required=True,
                    help="folder where plots are stored")

    args = vars(ap.parse_args())


    # makeHistogramForAllColumns('../../resources/orignaldataallfiles/All_files_combined.xlsx',
    #                            '../../resources/orignaldatakeys/catogoriesofkeys.xlsx',
    #                            action="save",inputfileformat='excel',keysfileformat='excel',
    #                            savefigaddress='../../resources/figuresorignaldata/histograms/')


    # --inputfile ../../resources/orignaldataallfiles/All_files_combined.xlsx --inputfilekeys ../../resources/orignaldatakeys/catogoriesofkeys.xlsx --inputformat excel --inputkeysformat excel --outputaction save --outputfolder ../../resources/figuresorignaldata/histograms/

    makeHistogramForAllColumns(args['inputfile'],args['inputfilekeys'], action=args['outputaction'],
                                           inputfileformat=args['inputformat'],keysfileformat=args['inputkeysformat'],savefigaddress= os.path.join(args['outputfolder'], ''))
