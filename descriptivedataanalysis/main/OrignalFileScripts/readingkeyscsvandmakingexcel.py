import os
import json
import pandas as pd
import logging
from jsonFlattner.flatten_json import flatten
from pandas import ExcelWriter
from pandas.io.common import EmptyDataError
import numpy as np
from main.util.utilfunctions import output_df_to_file
from main.util.utilfunctions import output_list_to_csv_file
from main.util.utilfunctions import pickle_list_to_file
pd.set_option('display.height', 10000)
pd.set_option('display.max_rows', 10000)
pd.set_option('display.max_columns', 10000)
pd.set_option('display.width', 10000)

# Create the Logger
logger = logging.getLogger("Original_Data_Key_Count")
logger.setLevel(logging.INFO)

logger_formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s')
logger_file_handler = logging.FileHandler('Orignal_data.log')
logger_file_handler.setFormatter(logger_formatter)
logger.addHandler(logger_file_handler)

logger_console_Handler = logging.StreamHandler()
logger_console_Handler.setFormatter(logger_formatter)
logger.addHandler(logger_console_Handler)
logger.info('Completed configuring logger()!')

inputDirPath = '../../resources/orignaldataallfiles/'
#inputDirPath = '../../resources/orignaldata/'
d = os.listdir(inputDirPath)
zz = None
s = set() # hold intersection of keys present in all rows all files
u = set() # hold union of all keys in all files
cnt_dict = {}
cnt = 0# no of rows in all files


for i in d:
    if i.endswith(".csv"):
        try:
            test = pd.read_csv(os.path.join(inputDirPath,i),sep=';')
        except:
            continue
        for i,j in test.iterrows():
            zz= json.loads(j['result_attributes'])
            if cnt ==0:
                s = set(zz.keys())
            cnt+=1
            t = set(zz.keys())

            s = s.intersection(t)
            u = u.union(t)
            for k in zz.keys():
                if k in cnt_dict:
                    cnt_dict[k]+=1
                else:
                    cnt_dict[k] = 1

output_list_to_csv_file(list(s),"./listofglobalkeys.csv")
pickle_list_to_file(list(s),"./listofglobalkeys")
exit()
#count the number of frequencies occuring in keys
cnt_count = {}
for i in cnt_dict:
    print (i, cnt_dict[i])
    if cnt_dict[i] in cnt_count:
        cnt_count[cnt_dict[i]] +=1
    else:
        cnt_count[cnt_dict[i]] =1


# check for value type not complete yet

df = pd.read_excel("../../resources/orignaldata/All_files_combined.xlsx")
for name in df.columns:
    if isinstance(df[name].dtype,dict):
        print("Its a dict")
    elif isinstance(df[name].dtype, list):
        print("Its a dict")
    else:
        print("It atomic")