import numpy as np; np.random.seed(sum(map(ord, "distributions")))
import seaborn as sns; sns.set(color_codes=True)
import pandas as pd
pd.set_option('display.height', 10000)
pd.set_option('display.max_rows', 10000)
pd.set_option('display.max_columns', 10000)
pd.set_option('display.width', 10000)
from pandas import ExcelWriter
import argparse

def calculateSupportForColumnsPairValues(inputFile, inputKeys, inputFileFormat, keyFileFormat, action='show'):
    df_in = get_dataframe_from_file(inputFile, inputFileFormat)
    df_keys = get_dataframe_from_file(inputKeys, keyFileFormat)

    #print(tabulate(df_in.head(15)))

    for col in  df_keys['boolkeys'].dropna():
        booleanDictionary = {1: 'TRUE', 0: 'FALSE'}
        df_in[col]=df_in[col].map(booleanDictionary)

    df_in = df_in.fillna('EMPTY')
    df_in = df_in[df_keys['atomickeys'].dropna()]

    for col in df_in.columns:
        if len(df_in[col].unique()) > 100:
            df_in.loc[df_in[col] != 'EMPTY', col] = "PRESENT"




    statsFrames = []
    noOfRowsInDF = len(df_in.index)
    ruleCounter=1
    for ind1,col1 in enumerate(df_in.columns):
        for ind2, col2 in enumerate(df_in.columns):
            if ind2 > ind1:

                col1Val = df_in[col1]
                col1ValFrequency = col1Val.value_counts()
                col1ValVariable = (noOfRowsInDF+2)/(col1ValFrequency+1)

                col1ValFrequency= col1ValFrequency.to_dict()
                col1ValVariable=col1ValVariable.to_dict()

                col2Val = df_in[col2]
                col2ValFrequency = col2Val.value_counts()
                col2ValVariable = (noOfRowsInDF+2)/(col2ValFrequency+1)
                col2ValFrequency= col2ValFrequency.to_dict()
                col2ValVariable = col2ValVariable.to_dict()

                df_new = df_in.groupby([col1,col2])[col2].count().unstack(fill_value=0).stack().reset_index(name="frequency of tuple")


                df_new['joint probability'] = ((df_new['frequency of tuple']+1)/(noOfRowsInDF+2))


                support = pd.Series()
                key1 =pd.Series()
                key2 = pd.Series()
                freqValue1 = pd.Series()
                freqValue2 = pd.Series()
                for index, row in df_new.iterrows():
                    ab= row['joint probability']
                    a= col1ValVariable[row[col1]]
                    b= col2ValVariable[row[col2]]
                    supportValue=( ab * a * b)
                    support.set_value(index, supportValue)
                    key1.set_value(index, col1)
                    key2.set_value(index, col2)
                    freqValue1.set_value(index, col1ValFrequency[row[col1]])
                    freqValue2.set_value(index, col2ValFrequency[row[col2]])

                df_new['key1'] = key1
                df_new['key2'] = key2
                df_new['support'] = support
                df_new['frequency value1'] = freqValue1
                df_new['frequency value2'] = freqValue2
                df_new['rule candidate'] = ruleCounter
                # print(df_new)
                df_new = df_new.drop(['joint probability'], axis=1)

                df_new.columns = ['value1', 'value2', 'frequency of tuple', 'key1', 'key2', 'R',
                                  'frequency value1', 'frequency value2','rule candidate']
                df_new = df_new[
                    ['key1', 'key2', 'value1', 'value2', 'frequency of tuple', 'R', 'frequency value1',
                     'frequency value2','rule candidate']]
                df_new = df_new.sort_values('R')
                statsFrames.append(df_new)
                ruleCounter+=1

    finalDF = pd.concat(statsFrames, ignore_index=True)
    if True:
        output_df_to_file(finalDF, '../../resources/orignaldataallfiles/Two_Tuple_Support_Main_File', 'csv')
        output_df_to_file(finalDF, '../../resources/orignaldataallfiles/Two_Tuple_Support_Main_File', 'excel')






def output_df_to_file(newDf, outputFilePath, outputFormat):

   if outputFormat.lower() == "csv":
        defaultCsvOutputFilePath = "".join([outputFilePath, ".", "csv"])
        newDf.to_csv(defaultCsvOutputFilePath, sep='\t', encoding='utf-8')

   else:
        defaultStatsExcelFilePath = "".join([outputFilePath, ".", "xlsx"])
        writer = ExcelWriter(defaultStatsExcelFilePath)
        newDf.to_excel(writer, 'Sheet1')
        writer.save()

def get_dataframe_from_file(inputFile,fileFormat):

    if fileFormat.lower() == 'csv':
        df= pd.read_csv(inputFile, index_col=0, sep='\t', encoding='utf-8')
    elif fileFormat.lower() == 'excel':
        df= pd.read_excel(inputFile)
    else:
        df= None
        print("file format should be csv or excel")

    return df



if __name__ == '__main__':
    # construct the argument parse and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--inputfile", required=True,
                    help="one input file in which all final files combined")
    ap.add_argument("-ik", "--inputfilekeys", required=True,
                    help="one input file for keys to be considered")

    ap.add_argument("-inf", "--inputformat", required=True,
                    help="format of input file <csv/xlsx>")
    ap.add_argument("-inkf", "--inputkeysformat", required=True,
                    help="format of keys input file <csv/xlsx>")



    args = vars(ap.parse_args())

   # calculateSupportForColumnsPairValues('../../resources/orignaldataallfiles/All_files_combined_small.xlsx', '../../resources/orignaldatakeys/catogoriesofkeys.xlsx','excel', 'excel',action='save')
   #--inputfile ../../resources/orignaldataallfiles/All_files_combined.xlsx --inputfilekeys ../../resources/orignaldatakeys/catogoriesofkeys.xlsx --inputformat excel --inputkeysformat excel
    calculateSupportForColumnsPairValues(args['inputfile'],args['inputfilekeys'], args['inputformat']
                                         , args['inputkeysformat'])