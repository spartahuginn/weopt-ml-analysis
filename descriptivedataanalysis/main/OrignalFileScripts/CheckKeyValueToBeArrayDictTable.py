import pandas as pd
import csv

def filter_list(full_list, excludes):
    s = set(excludes)
    return (x for x in full_list if x not in s)

with open('../../resources/orignaldatakeys/arrayentriesglobalkeys.csv', 'r') as f:
  reader = csv.reader(f)
  listsKeys = list(reader)

with open('../../resources/orignaldatakeys/dictenteriesglobalkeys.csv', 'r') as f:
  reader = csv.reader(f)
  dictsKeys = list(reader)

with open('../../resources/orignaldatakeys/tableenteriesglobalkeys.csv', 'r') as f:
  reader = csv.reader(f)
  tableKeys = list(reader)

with open('../../resources/orignaldatakeys/listofglobalkeys.csv', 'r') as f:
  reader = csv.reader(f)
  allKeys = list(reader)
  filteredlist =allKeys[0]
  filteredlist = list(filter_list(filteredlist, listsKeys[0]))
  filteredlist = list(filter_list(filteredlist, dictsKeys[0]))
  filteredlist = list(filter_list(filteredlist, tableKeys[0]))
  print(filteredlist)


  nonPlottableFields = ['create_time','base_obj_id','cad_description',
                        'create_time','created_orignal_data_file','em_addit_spec',
                        'em_gross_weight','em_mass','image','last_modification_time','obj_id',
                        'oem_code','owner_model_filename','sub_comp_codes',
                        'rev_date','rev_reason','technical_weight','transition_time']
  plottableFilteredList =  list(filter_list(filteredlist, nonPlottableFields))

  booleanFields = ['custom_material',
                   'generate_configuration',
                   'ignore_rels_in_conf',
                   'is_current_rev',
                   'is_latest_rev',
                   'is_latest_rev_in_state',
                   'is_old_rev',
                   'spare_indic',
                   'spares_bom'
                   ]


d = {"listkeys":listsKeys[0], "dictkeys":dictsKeys[0], "tablekeys":tableKeys[0], "atomickeys":filteredlist, "allkeys":allKeys[0], "plotatomickeys":plottableFilteredList, "boolkeys":booleanFields}



#df= pd.DataFrame({"listkeys":listsKeys, "dictkeys":dictsKeys, "tablekeys":tableKeys, "atomickeys":[filteredlist], "allkeys":allKeys} )

df =  pd.DataFrame(dict([ (k,pd.Series(v)) for k,v in d.items() ]))

df.to_excel("../../resources/orignaldatakeys/catogoriesofkeys.xlsx")

