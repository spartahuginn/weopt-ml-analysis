import pandas as pd
import csv
pd.set_option('display.height', 10000)
pd.set_option('display.max_rows', 10000)
pd.set_option('display.max_columns', 10000)
pd.set_option('display.width', 10000)
from pandas import ExcelWriter
import pickle



def output_df_to_file(newDf, outputFilePath, outputFormat):

    if outputFormat.lower() == "csv":
        defaultCsvOutputFilePath = "".join([outputFilePath, ".", "csv"])
        newDf.to_csv(defaultCsvOutputFilePath, sep='\t', encoding='utf-8')

    else:
        defaultStatsExcelFilePath = "".join([outputFilePath, ".", "xlsx"])
        writer = ExcelWriter(defaultStatsExcelFilePath)
        newDf.to_excel(writer, 'Sheet1')
        writer.save()

def output_list_to_csv_file(list, outputFilePath):
    with open(outputFilePath, 'w') as resultFile:
        wr = csv.writer(resultFile, dialect='excel')
        wr.writerow(list)


def pickle_list_to_file(list, outputFilePath):
    with open(outputFilePath, 'wb') as resultFile:
        pickle.dump(list, resultFile)