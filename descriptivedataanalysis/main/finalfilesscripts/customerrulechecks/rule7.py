import pandas as pd
from pandas import ExcelWriter

from main.util.utilfunctions import output_df_to_file


df = pd.read_csv('../../../resources/finaldata/Three_Tuple_Support_Main_File_sophisticated.csv', sep='|')
#df=df.reset_index().drop('index',axis=1)
df = df.fillna('EMPTY')
df_obj = df.select_dtypes(['object'])
df[df_obj.columns] = df_obj.apply(lambda x: x.str.strip())

mylist =['Current State','has part','sub_materials_released' ]

selectedRows = []
for index, row in df.iterrows():
    if (set(mylist) < set(row.values)):
        selectedRows.append(row)

selectedDf = pd.DataFrame(selectedRows)
selectedDf.columns = [x.strip() for x in selectedDf.columns]

#selectedDf =selectedDf.loc[selectedDf['val1'] == 'released' & str(selectedDf['val2'])==True]

output_df_to_file(selectedDf, './rule7', 'excel')



