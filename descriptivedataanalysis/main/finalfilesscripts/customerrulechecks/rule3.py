import pandas as pd
from pandas import ExcelWriter
from main.util.utilfunctions import output_df_to_file




df = pd.read_csv('../../../resources/finaldata/Two_Tuple_Support_Main_File_sophisticated.csv', sep='\t')
df=df.drop('Unnamed: 0',axis=1)
df = df.fillna('EMPTY')
df_obj = df.select_dtypes(['object'])
df[df_obj.columns] = df_obj.apply(lambda x: x.str.strip())

mylist =['KONE Type','has part' ]

selectedRows = []
for index, row in df.iterrows():
    if (set(mylist) < set(row.values)):
        selectedRows.append(row)

selectedDf = pd.DataFrame(selectedRows)
selectedDf.columns = [x.strip() for x in selectedDf.columns]

#selectedDf =selectedDf.loc[selectedDf['val1'] == 'released' & str(selectedDf['val2'])==True]
selectedDf=selectedDf.sort_values('support')
output_df_to_file(selectedDf, './rule3', 'excel')



