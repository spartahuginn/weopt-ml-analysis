import pandas as pd


rule2 = pd.read_excel('./rule2.xlsx', sheetname='Sheet1')
rule2['rulename'] = ["rule2" for i in rule2.index]

rule3 = pd.read_excel('./rule3.xlsx', sheetname='Sheet1')
rule3['rulename'] = ["rule3" for i in rule3.index]

rule4 = pd.read_excel('./rule4.xlsx', sheetname='Sheet1')
rule4['rulename'] = ["rule4" for i in rule4.index]

rule5 = pd.read_excel('./rule5.xlsx', sheetname='Sheet1')
rule5['rulename'] = ["rule5" for i in rule5.index]

frames= [rule2,rule3,rule4, rule5]
result = pd.concat(frames, ignore_index=True)

result.to_excel(pd.ExcelWriter('./similarformatrulesmerged.xlsx'), 'Sheet1')


rule7 = pd.read_excel('./rule7.xlsx', sheetname='Sheet1')
rule7['rulename'] = ["rule7" for i in rule7.index]


