import os
import pandas as pd
pd.set_option('display.height', 10000)
pd.set_option('display.max_rows', 10000)
pd.set_option('display.max_columns', 10000)
pd.set_option('display.width', 10000)
import argparse
import matplotlib.pyplot as plt
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})


def makeHistogramAccumulativeFilesStats(inputFile, fileFormat,outputfolder,action='save'):
    dframe = get_dataframe_from_file(inputFile,fileFormat)
    dframe = dframe.T
    dframe[['value counts','key counts']].plot(kind='bar', title ="Sparta", figsize=(60, 15), legend=True, fontsize=12)
    if action.lower() == 'show':

        plt.show()
    else:
        fig = plt.gcf()
        fig.savefig("".join([outputfolder,"Key Value Accumulative",".png"]),dpi=600)
        plt.close(fig)

def get_dataframe_from_file(inputFile,fileFormat):

    if fileFormat.lower() == 'csv':
        df= pd.read_csv(inputFile, index_col=0,sep='\t', encoding='utf-8')
    elif fileFormat.lower() == 'excel':
        df= pd.read_excel(inputFile)
    else:
        df= None
        print("file format should be csv or excel")

    return df


if __name__ == '__main__':
    # construct the argument parse and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--inputfile", required=True,
                    help="the files should be Accumulative_files_stats.<csv/xlsx>")

    ap.add_argument("-inf", "--inputformat", required=True,
                    help="format of input file <csv/xlsx>")

    ap.add_argument("-o", "--outputaction", required=True,
                    help="save plots as pics or draw plots on screen. <show/save>")

    ap.add_argument("-outf", "--outputfolder", required=True,
                    help="folder where plots are stored")

    args = vars(ap.parse_args())


    makeHistogramAccumulativeFilesStats(args['inputfile'], args['inputformat'],os.path.join(args['outputfolder'], ''), action = args['outputaction'])

    #--inputfile ../../resources/inputfiles/accumulative_files_stats.xlsx --inputformat excel --outputaction save --outputfolder ../../resources/figures/histograms/