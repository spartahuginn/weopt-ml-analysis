import os
import numpy as np; np.random.seed(sum(map(ord, "distributions")))
import seaborn as sns; sns.set(color_codes=True)
import pandas as pd
pd.set_option('display.height', 10000)
pd.set_option('display.max_rows', 10000)
pd.set_option('display.max_columns', 10000)
pd.set_option('display.width', 10000)
from pandas import ExcelWriter
import argparse
import numpy as np
import matplotlib.ticker as plticker
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})
plt.style.use('seaborn-whitegrid')
SMALL_SIZE = 12
MEDIUM_SIZE = 15
BIGGER_SIZE = 24

plt.rc('font', size=BIGGER_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=BIGGER_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title



def calculateSupportForColumnsPairValues(inputFile, fileFormat,outputFolder, cols_of_interest):

    df_in = get_dataframe_from_file(inputFile,fileFormat)

    df_in = df_in[cols_of_interest]
    df_in = df_in.fillna("EMPTY")
    df_in.loc[df_in['Material Utilisation Group'] != 'EMPTY', 'Material Utilisation Group'] = "PRESENT"

    statsFrames = []
    noOfRowsInDF = len(df_in.index)
    ruleCounter=1
    for ind1,col1 in enumerate(df_in.columns):
        for ind2, col2 in enumerate(df_in.columns):
            if ind2 > ind1:

                col1Val = df_in[col1]
                col1ValFrequency = col1Val.value_counts()
                col1ValVariable = (noOfRowsInDF + 2) / (col1ValFrequency + 1)

                col1ValFrequency = col1ValFrequency.to_dict()
                col1ValVariable = col1ValVariable.to_dict()

                col2Val = df_in[col2]
                col2ValFrequency = col2Val.value_counts()
                col2ValVariable = (noOfRowsInDF + 2) / (col2ValFrequency + 1)
                col2ValFrequency = col2ValFrequency.to_dict()
                col2ValVariable = col2ValVariable.to_dict()

                df_new = df_in.groupby([col1, col2])[col2].count().unstack(fill_value=0).stack().reset_index(
                    name="frequency of tuple")

                df_new['joint probability'] = ((df_new['frequency of tuple'] + 1) / (noOfRowsInDF + 2))

                support = pd.Series()
                key1 = pd.Series()
                key2 = pd.Series()
                freqValue1 = pd.Series()
                freqValue2 = pd.Series()
                for index, row in df_new.iterrows():
                    ab = row['joint probability']
                    a = col1ValVariable[row[col1]]
                    b = col2ValVariable[row[col2]]
                    supportValue = (ab * a * b)
                    support.set_value(index, supportValue)
                    key1.set_value(index, col1)
                    key2.set_value(index, col2)
                    freqValue1.set_value(index, col1ValFrequency[row[col1]])
                    freqValue2.set_value(index, col2ValFrequency[row[col2]])

                df_new['key1'] = key1
                df_new['key2'] = key2
                df_new['support'] = support
                df_new['frequency value1'] = freqValue1
                df_new['frequency value2'] = freqValue2
                df_new['rule candidate'] = ruleCounter
                # print(df_new)
                df_new = df_new.drop(['joint probability'], axis=1)

                df_new.columns = ['value1', 'value2', 'frequency of tuple', 'key1', 'key2', 'R',
                                  'frequency value1', 'frequency value2','rule candidate']
                df_new = df_new[
                    ['key1', 'key2', 'value1', 'value2', 'frequency of tuple', 'R', 'frequency value1',
                     'frequency value2','rule candidate']]
                df_new = df_new.sort_values('R')
                statsFrames.append(df_new)
                ruleCounter+=1

    finalDF = pd.concat(statsFrames, ignore_index=True)
    if True:
        output_df_to_file(finalDF, "".join([outputFolder,'Two_Tuple_Support_Main_File']), 'csv')
        output_df_to_file(finalDF, "".join([outputFolder,'Two_Tuple_Support_Main_File']), 'excel')





def output_df_to_file(newDf, outputFilePath, outputFormat):

   if outputFormat.lower() == "csv":
        defaultCsvOutputFilePath = "".join([outputFilePath, ".", "csv"])
        newDf.to_csv(defaultCsvOutputFilePath, sep='\t', encoding='utf-8')

   else:
        defaultStatsExcelFilePath = "".join([outputFilePath, ".", "xlsx"])
        writer = ExcelWriter(defaultStatsExcelFilePath)
        newDf.to_excel(writer, 'Sheet1')
        writer.save()

def get_dataframe_from_file(inputFile,fileFormat):

    if fileFormat.lower() == 'csv':
        df= pd.read_csv(inputFile, index_col=0, sep='\t', encoding='utf-8')
    elif fileFormat.lower() == 'excel':
        df= pd.read_excel(inputFile)
    else:
        df= None
        print("file format should be csv or excel")

    return df



if __name__ == '__main__':
    cols_of_interest = ['Current State', 'Design Origin', 'KONE Type',
                        'Level of Pre-engineering', 'Material Utilisation Group',
                        'Unit of measure','characteristics', 'has part', 'spare_part',
                        'sub_materials_released']

    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--inputfile", required=True,
                    help="the file should be All_files_combined.<csv/xlsx>")

    ap.add_argument("-inf", "--inputformat", required=True,
                    help="format of input file <csv/xlsx>")

    ap.add_argument("-outf", "--outputfolder", required=True,
                    help="folder where plots are stored")

    args = vars(ap.parse_args())

    calculateSupportForColumnsPairValues(args['inputfile'], args['inputformat'],os.path.join(args['outputfolder'], ''), cols_of_interest)
    #--inputfile ../../resources/finaldata/inputfiles/All_files_combined.csv --inputformat csv --outputfolder ../../resources/finaldata/inputfiles/