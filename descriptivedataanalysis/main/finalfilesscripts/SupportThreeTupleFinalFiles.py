# coding: utf-8

# In[ ]:
import os
import pandas as pd
import numpy as np
from  itertools import combinations
from pandas import ExcelWriter
import argparse


def calculateSupportForColumnsPairValues(inputFile, fileFormat, outputFolder, cols_of_interest):
    # In[2]:
    res = get_dataframe_from_file(inputFile, fileFormat)
    res = res[cols_of_interest + [u'Unnamed: 0']]
    res = res.fillna("EMPTY")
    res.loc[res['Material Utilisation Group'] != 'EMPTY', 'Material Utilisation Group'] = "PRESENT"
    # Alex
    # res1 = pd.read_csv('../../resources/finaldata/inputfiles/All_files_combined.csv', delimiter='\t').replace(np.nan, 'EMPTY',
    #                                                                                             regex=True)
    # In[5]:

    # compute combinations (choose 2 possible values from the lsit)
    # [u'Current State', u'Design Origin', u'KONE Type', u'Level of Pre-engineering', u'Unit of measure',u'characteristics', u'has part', u'spare_part', u'sub_materials_released']
    cc = list(combinations(cols_of_interest, 2))

    # create count of all tuples including empty ones e.g.
    # Current State|Design Origin|checked|COMM|33
    # Current State|Design Origin|checked|EMPTY|2

    ds_arr = []
    for i in cc:
        r = res.groupby(i)[i[1]].count().unstack(fill_value=0).stack().reset_index()

        for j, k in r.iterrows():
            # print("%s|%s|%s|%s|%s" % (r.columns[0], r.columns[1], k[r.columns[0]], k[r.columns[1]], k[0]))

            # lines below create a dictionary, which I later convert to pandas dataframe
            t = {'k1': r.columns[0], 'k2': r.columns[1], 'v1': str(k[r.columns[0]]), 'v2': str(k[r.columns[1]]),
                 'count': k[0]}

            ds_arr.append(t)
    final_pair_cnt = pd.DataFrame(ds_arr)

    # count of single values, and conversion to "relational" format: (key,value,count)
    single_cnt = []
    # alex
    # [u'Current State', u'Design Origin', u'KONE Type', u'Level of Pre-engineering', u'Unit of measure',u'characteristics', u'has part', u'spare_part', u'sub_materials_released']
    for i in cols_of_interest:
        r1 = res.groupby(i).size().to_frame().reset_index()  # this expression computes counts
        for j, k in r1.iterrows():
            # print(r1.columns[0], k[r1.columns[0]], k[0])
            t = {'k': r1.columns[0], 'v': str(k[r1.columns[0]]), 'cnt': k[0]}
            single_cnt.append(t)
    final_one_cnt = pd.DataFrame(single_cnt)

    # In[13]:

    dbl = []

    N = len(res)

    # N = 35992.0

    def output_df_to_file(newDf, outputFilePath, outputFormat):

        if outputFormat.lower() == "csv":
            defaultCsvOutputFilePath = "".join([outputFilePath, ".", "csv"])
            newDf.to_csv(defaultCsvOutputFilePath, sep='\t', encoding='utf-8')

        else:
            defaultStatsExcelFilePath = "".join([outputFilePath, ".", "xlsx"])
            writer = ExcelWriter(defaultStatsExcelFilePath)
            newDf.to_excel(writer, 'Sheet1')
            writer.save()

    def get_tuple_count(df, k_tuple, v_tuple):
        # computes count of tuple from parameters. k_tuple = pair of keys, v_tuple = pair of values (for those keys)
        try:
            cnt = df.loc[(df['k1'] == k_tuple[0]) & (df['v1'] == v_tuple[0]) & (df['k2'] == k_tuple[1]) & (
                df['v2'] == v_tuple[1])]['count'].iloc[0]
        except:
            cnt = df.loc[(df['k1'] == k_tuple[1]) & (df['v1'] == v_tuple[1]) & (df['k2'] == k_tuple[0]) & (
                df['v2'] == v_tuple[0])]['count'].iloc[0]
        return cnt

    # alex
    # [u'Current State', u'Design Origin', u'KONE Type', u'Level of Pre-engineering', u'Unit of measure',u'characteristics', u'has part', u'spare_part', u'sub_materials_released']
    cc = list(combinations(cols_of_interest, 3))

    #print('key1', '|', 'key2', '|', 'key3', '|', 'val1', '|', 'val2', '|', 'val3', '|', 'freq', '|', 'R1', '|', 'R2',
    #     '|','R3')  # , tc1, tc2, tc3

    finalthreetuplelist = []
    for i in cc:
        # ALEX
        r = res.groupby(i)[i[1]].count().unstack(fill_value=0).stack().reset_index()
        # r= key1,key2,key3,count
        new_index = pd.MultiIndex.from_product([res[i[0]].unique(), res[i[1]].unique(), res[i[2]].unique()],
                                               names=list(i))
        col = list(i)
        t1 = res.groupby(col).count().reindex(new_index).reset_index()
        col += [u'Unnamed: 0']
        r = t1[col].fillna(0)

        for j, k in r.iterrows():
            # here I use same code in "t1" "t2" and "t3" : idea is that I compute all combinations made of 2 and 1 elements in 3 elements
            # for example (A,B,C) would be (A,B),(C); (A,C),(B), etc
            # t1
            i1 = 0
            i2 = 1
            i3 = 2
            t = (r.columns[i1], str(r.columns[i2]))  # tuple(key1,key2)
            v = (str(k[r.columns[i1]]), str(k[r.columns[i2]]))  # tuple(value1,value2)
            tc = get_tuple_count(final_pair_cnt, t, v)  # tuple(v1,v2) count

            # value3 count
            s_c = \
                final_one_cnt.loc[
                    (final_one_cnt['k'] == r.columns[i3]) & (final_one_cnt['v'] == str(k[str(r.columns[i3])]))][
                    'cnt'].iloc[0]
            tc1 = tc
            R1 = (k['Unnamed: 0'] + 1) / (N + 2) * (N + 2) / (tc + 1) * (N + 2) / (s_c + 1)
            # t2
            i1 = 0
            i2 = 2
            i3 = 1
            t = (r.columns[i1], r.columns[i2])
            v = (str(k[r.columns[i1]]), str(k[r.columns[i2]]))
            tc = get_tuple_count(final_pair_cnt, t, v)
            s_c = \
                final_one_cnt.loc[
                    (final_one_cnt['k'] == r.columns[i3]) & (final_one_cnt['v'] == str(k[str(r.columns[i3])]))][
                    'cnt'].iloc[0]
            tc2 = tc
            R2 = (k['Unnamed: 0'] + 1) / (N + 2) * (N + 2) / (tc + 1) * (N + 2) / (s_c + 1)
            # t3

            i1 = 2
            i2 = 1
            i3 = 0
            t = (r.columns[i1], r.columns[i2])
            v = (str(k[r.columns[i1]]), str(k[r.columns[i2]]))
            tc = get_tuple_count(final_pair_cnt, t, v)
            s_c = \
                final_one_cnt.loc[
                    (final_one_cnt['k'] == r.columns[i3]) & (final_one_cnt['v'] == str(k[str(r.columns[i3])]))][
                    'cnt'].iloc[0]
            tc3 = tc
            R3 = (k['Unnamed: 0'] + 1) / (N + 2) * ((N + 2) / (tc + 1)) * ((N + 2) / (s_c + 1))
            # if R1 <=5 and R2 <= 5 and R3 <= 5 and k[0] <= 40000:
            # print(r.columns[0], '|', r.columns[1], '|', r.columns[2], '|', k[r.columns[0]], '|', k[r.columns[1]], '|',
            #       k[r.columns[2]], '|', k[0], '|', R1, '|', R2, '|', R3)  # , tc1, tc2, tc3
            # dbl.append(k[0])
            finalthreetuplelist.append({
                'key1': r.columns[0], 'key2': r.columns[1], 'key3': r.columns[2],
                'val1': k[r.columns[0]], 'val2': k[r.columns[1]], 'val3': k[r.columns[2]],
                'freq': k['Unnamed: 0'], 'R1': R1, 'R2': R2, 'R3': R3, })
            '''
            print({
            'key1':r.columns[0], 'key2':r.columns[1], 'key3':r.columns[2],
            'val1':k[r.columns[0]], 'val2':k[r.columns[1]], 'val3':k[r.columns[2]],
            'freq':k['Unnamed: 0'], 'R1':R1, 'R2':R2,'R3':R3,})
            '''

    # In[ ]:

    finalthreetuplelistdf = pd.DataFrame(finalthreetuplelist)
    finalthreetuplelistdf = finalthreetuplelistdf[
        ['key1', 'key2', 'key3', 'val1', 'val2', 'val3', 'freq', 'R1', 'R2', 'R3']]

    # print(finalthreetuplelistdf.head(5))
    output_df_to_file(finalthreetuplelistdf, "".join([outputFolder, 'Three_Tuple_Support_Main_File']),
                      'excel')
    output_df_to_file(finalthreetuplelistdf, "".join([outputFolder, 'Three_Tuple_Support_Main_File']),
                      'csv')


def output_df_to_file(newDf, outputFilePath, outputFormat):
    if outputFormat.lower() == "csv":
        defaultCsvOutputFilePath = "".join([outputFilePath, ".", "csv"])
        newDf.to_csv(defaultCsvOutputFilePath, index_col=0, sep='\t', encoding='utf-8')

    else:
        defaultStatsExcelFilePath = "".join([outputFilePath, ".", "xlsx"])
        writer = ExcelWriter(defaultStatsExcelFilePath)
        newDf.to_excel(writer, 'Sheet1')
        writer.save()


def get_dataframe_from_file(inputFile, fileFormat):
    if fileFormat.lower() == 'csv':
        df = pd.read_csv(inputFile, sep='\t', encoding='utf-8')
    elif fileFormat.lower() == 'excel':
        df = pd.read_excel(inputFile)
    else:
        df = None
        print("file format should be csv or excel")

    return df


if __name__ == '__main__':
    cols_of_interest = ['Current State', 'Design Origin', 'KONE Type',
                        'Level of Pre-engineering', 'Material Utilisation Group',
                        'Unit of measure', 'characteristics', 'has part', 'spare_part',
                        'sub_materials_released']

    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--inputfile", required=True,
                    help="the file should be All_files_combined.<csv/xlsx>")

    ap.add_argument("-inf", "--inputformat", required=True,
                    help="format of input file <csv/xlsx>")

    ap.add_argument("-outf", "--outputfolder", required=True,
                    help="folder where plots are stored")

    args = vars(ap.parse_args())

    calculateSupportForColumnsPairValues(args['inputfile'], args['inputformat'], os.path.join(args['outputfolder'], ''), cols_of_interest)
    # --inputfile ../../resources/finaldata/inputfiles/All_files_combined.csv --inputformat csv --outputfolder ../../resources/finaldata/inputfiles/
