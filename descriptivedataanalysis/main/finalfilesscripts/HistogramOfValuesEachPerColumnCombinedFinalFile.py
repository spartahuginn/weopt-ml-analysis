import os
import pandas as pd
pd.set_option('display.height', 10000)
pd.set_option('display.max_rows', 10000)
pd.set_option('display.max_columns', 10000)
pd.set_option('display.width', 10000)
import argparse
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})


def makeHistogramForAllColumns(inputFile, fileFormat,outputfolder,action='save'):
    df_in = get_dataframe_from_file(inputFile,fileFormat)



    approvedBy = df_in['Approved by'].replace(np.nan, 'NULL', regex=True).value_counts()
    checkedBy= df_in['Checked by'].replace(np.nan, 'NULL', regex=True).value_counts()
    createdBy= df_in['Created by'].replace(np.nan, 'NULL', regex=True).value_counts()
    currentState= df_in['Current State'].replace(np.nan, 'NULL', regex=True).value_counts()
    designOrigin= df_in['Design Origin'].replace(np.nan, 'NULL', regex=True).value_counts()
    koneType= df_in['KONE Type'].replace(np.nan, 'NULL', regex=True).value_counts()
    #lastmodificationtime
    levelOfPreengineering = df_in['Level of Pre-engineering'].replace(np.nan, 'NULL', regex=True).value_counts()
    materialUtilizationGroup = df_in['Material Utilisation Group'].replace(np.nan, 'NULL', regex=True).value_counts()
    objectid= df_in['Object ID'].replace(np.nan, 'NULL', regex=True).value_counts()

    unitOfMeasure = df_in['Unit of measure'].replace(np.nan, 'NULL', regex=True).value_counts()
    characteristics = df_in['characteristics'].replace(np.nan, 'NULL', regex=True).value_counts()
    #createdfinaldatafile
    hasPart = df_in['has part'].replace(np.nan, 'NULL', regex=True).value_counts()
    materialid = df_in['material_id'].replace(np.nan, 'NULL', regex=True).value_counts()
    sparePart =df_in['spare_part'].replace(np.nan, 'NULL', regex=True).value_counts()
    materialRealeased = df_in['sub_materials_released'].replace(np.nan, 'NULL', regex=True).value_counts()
    if 31>21:
        plot_string_value_count(approvedBy,"Approved By",outputfolder,action)
        plot_string_value_count(checkedBy,"Checked By",outputfolder,action)
        plot_string_value_count(createdBy,"Created By",outputfolder,action)
        plot_string_value_count(currentState,"Current State",outputfolder,action)
        plot_string_value_count(designOrigin,"Design Origin",outputfolder,action)
        plot_string_value_count(koneType,"KONE Type",outputfolder,action)
        plot_string_value_count(levelOfPreengineering,"Level Of Pre-engineering",outputfolder,action)
        plot_string_value_count(materialUtilizationGroup,"Material Utilisation Group",outputfolder,action)
        #plot_string_value_count(objectid, "Object Id")
        plot_string_value_count(unitOfMeasure, " Unit Of measure",outputfolder,action)
        plot_string_value_count(characteristics, "characteristics",outputfolder,action)
        plot_string_value_count(hasPart, "has Part",outputfolder,action)
        #plot_string_value_count(materialid, "Material Id")
        plot_string_value_count(sparePart, "spare_part",outputfolder,action)
        plot_string_value_count(materialRealeased, "sub_material_released",outputfolder,action)

# ['Approved by' 'Checked by' 'Created by' 'Current State' 'Design Origin'
#  'KONE Type' 'Last modification time' 'Level of Pre-engineering'
#  'Material Utilisation Group' 'Object ID' 'Unit of measure'
#  'characteristics' 'created_final_data_file' 'has part' 'material_id'
#  'spare_part' 'sub_materials_released']

def plot_string_value_count(countSeries,plotTitle,outputfolder,action):
    #print("plotting " + plotTitle)

    values = countSeries.index.tolist()
    valuesOccurances = countSeries.values
    # bars are by default width 0.8, so we'll add 0.1 to the left coordinates
    # so that each bar is centered
    xs = [i + 0.1 for i, _ in enumerate(values)]
    plt.figure(figsize=(25, 15))

    # plot bars with left x-coordinates [xs], heights [valuesOccurances]
    plt.bar(xs, valuesOccurances,color='blue')
    plt.ylabel("# of Value Occurances")
    plt.title(plotTitle + " - " + " Distinct Items " + str(len(valuesOccurances)))
    # label x-axis with movie names at bar centers
    plt.xticks([i + 0.1 for i, _ in enumerate(values)], values, rotation='vertical', fontsize = 8)
    # Pad margins so that markers don't get clipped by the axes
    plt.margins(0.01)
    # Tweak spacing to prevent clipping of tick-labels
    plt.subplots_adjust(bottom=0.15)

    if action.lower() == 'show':
        mng = plt.get_current_fig_manager()
        mng.window.showMaximized()
        plt.show()
    else:
        fig = plt.gcf()
        fig.savefig("".join([outputfolder,plotTitle,".png"]),dpi=600)
        plt.close(fig)



def get_dataframe_from_file(inputFile,fileFormat):

    if fileFormat.lower() == 'csv':
        df= pd.read_csv(inputFile, index_col=0, sep='\t', encoding='utf-8')
    elif fileFormat.lower() == 'excel':
        df= pd.read_excel(inputFile)
    else:
        df= None
        print("file format should be csv or excel")

    return df

def makeHistogramForAllColumnsUsingPandas(inputFile, fileFormat):
    df_in = get_dataframe_from_file(inputFile,fileFormat)
    df_in.fillna("NULL")
    df_in['Approved by'].value_counts().plot(kind='bar')

    df_in['Checked by'].value_counts().plot(kind='bar')
    df_in['Created by'].value_counts().plot(kind='bar')
    df_in['Current State'].value_counts().plot(kind='bar')
    df_in['Design Origin'].value_counts().plot(kind='bar')
    df_in['KONE Type'].value_counts().plot(kind='bar')
    #lastmodificationtime
    df_in['Level of Pre-engineering'].value_counts().plot(kind='bar')
    df_in['Material Utilisation Group'].value_counts().plot(kind='bar')
    df_in['Object ID'].value_counts().plot(kind='bar')

    df_in['Unit of measure'].value_counts().plot(kind='bar')
    df_in['characteristics'].value_counts().plot(kind='bar')
    #createdfinaldatafile
    df_in['has part'].value_counts().plot(kind='bar')
    df_in['material_id'].value_counts().plot(kind='bar')
    df_in['spare_part'].value_counts().plot(kind='bar')
    df_in['sub_materials_released'].value_counts().plot(kind='bar')

    #print(df_in.columns.values)
    plt.show()



if __name__ == '__main__':
    # construct the argument parse and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--inputfile", required=True,
                    help="the file should be All_files_combined.<csv/xlsx>")

    ap.add_argument("-inf", "--inputformat", required=True,
                    help="format of input file <csv/xlsx>")

    ap.add_argument("-o", "--outputaction", required=True,
                    help="save plots as pics or draw plots on screen. <show/save>")

    ap.add_argument("-outf", "--outputfolder", required=True,
                    help="folder where plots are stored")

    args = vars(ap.parse_args())
    #makeHistogramForAllColumns('../../resources/finaldata/All_files_combined.xlsx', 'excel', "save")
    #--inputfile ../../resources/finaldata/inputfiles/All_files_combined.csv --inputformat csv --outputaction save --outputfolder ../../resources/figures/histograms/
    makeHistogramForAllColumns(args['inputfile'], args['inputformat'],os.path.join(args['outputfolder'], ''), action = args['outputaction'])